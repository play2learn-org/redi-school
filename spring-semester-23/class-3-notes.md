# Loop - die Schleife

Zusammenfassung der Aktivitäten des letzten Heroes Trainings zu Loops.

## Was ist eine Schleife

Manchmal gibt es Schritte, die sich wiederholen. Diese immer wieder einzeln aufzuschreiben ist anstrengend. Ab einer gewissen Anzahl ist es sogar fast unmöglich. Manchmal ist es auch gar nicht klar, wie oft sich diese Schritte wiederholen und erst während des Ablaufs erfährt man, wann sich die Wiederholung ändert.

Dann braucht man ein Konstrukt namens Schleife. Eine Schleife ist ein Block aus einem Kopf, manchmal einem Fuß-Element, und mehreren inneren Blöcken.

Z.B.

```
Wiederhole:
 Befehl 1
 Befehl 2
 Befehl 3

Zähle `Index` von 0 bis 255:
 Befehl 1 mit Variable Index
 Befehl 2 mit Variable Index
 Befehl 3 mit Variable Index

Solange Bedingung=Wahr, wiederhole:
 Befehl 1
 Befehl 2
 Befehl 3
```


## Stoppuhr

Wir wollen ein neues Spiel programmieren. In diesem Spiel soll eine Zahl dargestellt werden. Jede Sekunde wird diese Zahl um 1 erhöht. Drückt man einen Kopf, dann bleibt die Zahl auf ihrem aktuellen Stand.

## Das Fang Spiel

Ein Ball bewegt sich über den Bildschirm. Erst nach rechts, dann nach links.
Wenn er genau in der Mitte ist und man einen Knopf drückt, dann erhält der Spieler einen Punkt. Drückt man den Knopf während der Ball nicht in der Mitte ist, dann ist das Spiel vorbei und man bekommt seine Gesamtpunktzahl angezeigt.

## Verschiedene Arten von Schleifen

Zwei Schleifen fühlen sich für die meisten Menschen sehr natürlich an.

Die erste nennt sich "While", oder auf deutsch "Solange". In dieser Schleife, wird der Inhalt so lange wiederholt, wie die Bedingung wahr ist. Z.B. "Male so lange eine Linie wie du noch Papier hast." In diesem Beispiel würde man aufhören zu Malen, wenn man am Blattrand ist.

So sieht das als Code aus:

```
Solange PapierUebrig > 0:
  male_linie()
```

Eine andere übliche Schleife ist die Zählschleife. In dieser zählt das Programm automatisch eine Variable bis zu einem bestimmten Wert hoch. Ein Beispiel dafür haben wir schon weiter oben gesehen.

Es gibt aber auch ungewöhnlichere Schleifen. Z.B. könnnte man eine Schleife für immer laufen lassen, wenn die Bedingung immer wahr ist. z.B. werden wir immer darauf antworten, wenn jemand hallo zu uns sagt.

```
Solange Wahr:
  wenn gehörtes == "Hallo", dann:
    sage("Hallo")
```

Später lernen wir auch noch kennen, wie man sich im Programm Nachrichten schicken kann, und wie man auf diese Nachrichten reagiert. Das ist dann so ähnlich wie das Hallo sagen.

## Ausfüll Spiel

Für alle, die mit den vorherigen Spielen fertig sind, gibt es noch ein Spiel zum programmieren: Von oben links nach oben rechts, Zeile für Zeile, fülle das ganze Bild mit roten Kästchen.

Was ist hier besonderes zu beachten?
